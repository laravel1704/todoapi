<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function register(Request $request): JsonResponse
{
    $validator = Validator::make($request->all(), [
        'name' => 'required|regex:/^[a-zA-Z .]+$/',
        'email' => 'required|regex:/^[a-zA-Z0-9]+@[a-zA-Z0-9-]+(\.[a-zA-Z]+)$/|unique:users,email',
        'password' => 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$*&])[A-Za-z\d@$*&]{8,}$/',
    ], [
        'name.required' => 'The name field is required.',
        'name.regex' => 'The name field contains invalid characters.',
        'email.required' => 'The email field is required.',
        'email.regex' => 'Invalid email format.',
        'email.unique' => 'The email has already been taken.',
        'password.required' => 'The password field is required.',
        'password.regex' => 'The password field must contain at least 1 lowercase letter, 1 uppercase letter, 1 digit, 1 special character (@$*&), and be at least 8 characters long.',
    ]);

    if ($validator->fails()) {
        return response()->json(['status' => false, 'message' => $validator->errors()], 200);
    }

    $user = User::create([
        'name' => $request->name,
        'email' => $request->email,
        'password' => Hash::make($request->password),
    ]);

    return response()->json(['status' => true, 'message' => 'User registered successfully'], 201);
}


public function login(Request $request): JsonResponse
{
    $validator = Validator::make($request->all(), [
        'email' => 'required|regex:/^[a-zA-Z0-9]+@[a-zA-Z0-9-]+(\.[a-zA-Z]+)$/',
        'password' => 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$*&])[A-Za-z\d@$*&]{8,}$/',
    ], [
        'email.required' => 'The email field is required.',
        'email.regex' => 'Invalid email format.',
        'password.required' => 'The password field is required.',
        'password.regex' => 'Invalid Password',
    ]);

        if ($validator->fails()) {
        return response()->json(['status' => false, 'message' => $validator->errors()], 200);
    }

    $credentials = $request->only('email', 'password');

    if (!Auth::attempt($credentials)) {
        return response()->json(['status' => false, 'message' => 'The credentials you provided are incorrect.'], 200);
    }

    $user = $request->user();
    $token = $user->createToken('auth_token')->plainTextToken;

    return response()->json(['status' => true, 'token' => $token]);
}
}
