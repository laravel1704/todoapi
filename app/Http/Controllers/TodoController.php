<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{
    public function index(Request $request)
    {
        $todos = $request->user()->todos()->select('id', 'user_id', 'title', 'description', 'completed', 'created_at')->get();

        return response()->json(['status' => true,'message'=>'Todos fetched successfully', 'data' => $todos]);
    }


    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['status' => false, 'message' => $errors[0]], 200);
        }

        $request->user()->todos()->create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        return response()->json(['status' => true, 'message' => 'Todo created successfully']);
    }



    public function show(Request $request, $id): JsonResponse
    {
        if ($id) {
            $todo = $request->user()->todos()->select('id', 'user_id', 'title', 'description', 'completed', 'created_at')->find($id);

            if ($todo) {
                return response()->json(['status' => true, 'message' => 'Todo fetched successfully', 'data' => $todo]);
            } else {
                return response()->json(['status' => false, 'message' => 'Todo not found for the provided ID']);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Enter the ID you want to fetch']);
        }
    }


    public function update(Request $request, $id): JsonResponse
    {
        $request->validate([
            'title' => 'nullable|string',
            'description' => 'nullable|string',
            'completed' => 'boolean',
        ]);

        if ($id) {
            $todo = $request->user()->todos()->find($id);

            if ($todo) {
                $todo->update($request->only(['title', 'description', 'completed']));

                return response()->json(['status' => true, 'message' => 'Todo updated successfully']);
            } else {
                return response()->json(['status' => false, 'message' => 'Todo not found for the provided ID'], 404);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Enter the ID to be updated']);
        }
    }
    public function destroy(Request $request, $id): JsonResponse
    {
        if ($id) {
            $todo = $request->user()->todos()->find($id);

            if ($todo) {
                $todo->delete();

                return response()->json(['status' => true, 'message' => 'Todo deleted successfully']);
            } else {
                return response()->json(['status' => false, 'message' => 'Todo not found for the provided ID']);
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Enter the ID to be deleted']);
        }
    }
}
